﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> promocodesRepository;
        private readonly IRepository<Preference> preferenceRepository;
        private readonly IRepository<Customer> customerRepository;

        public PromocodesController(IRepository<PromoCode> promocodesRepository, IRepository<Preference> preferenceRepository, IRepository<Customer> customerRepository)
        {
            this.promocodesRepository = promocodesRepository;
            this.preferenceRepository = preferenceRepository;
            this.customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var promocodes = await promocodesRepository.GetAllAsync();

            var responses = promocodes.Select(x=>
            new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString(),
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo,
            }).ToList();
            return Ok(responses);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await preferenceRepository.GetAll().FirstOrDefaultAsync(p => p.Name == request.Preference);
           // var preferences = await preferenceRepository.GetAllAsync();
           // var preference = preferences.FirstOrDefault(p => p.Name == request.Preference);
            if (preference == null)
            {
                return BadRequest("В базе нет предпочтения "+request.Preference+"!");
            }
            
            var customers = await customerRepository.GetAllAsync();
            var customersWithPreference = customers.Where(p => p.Preferences.Contains(preference)).ToList();
            foreach (var customer in customersWithPreference)
            {
                // Если у пользователя уже есть такой промокод, пропускаем
                if (customer.PromoCodes.Any(p => p.Code == request.PromoCode))
                    continue;

                // Иначе создаём и выдаём сразу этому пользователю
                await promocodesRepository.AddAsync(
                   new PromoCode()
                   {
                       Code = request.PromoCode,
                       ServiceInfo = request.ServiceInfo,
                       PartnerName = request.PartnerName,
                       Preference = preference,
                       Customer = customer                       
                   });
            }
            return Ok();
        }
    }
}