﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Preference> preferencesRepository;

        public CustomersController(IRepository<Customer> customerRepository,IRepository<Preference> preferencesRepository)
        {
            this.customerRepository = customerRepository;
            this.preferencesRepository = preferencesRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {         
            var customers = await customerRepository.GetAllAsync();
            var customersModelList = customers.Select(x =>
               new CustomerShortResponse()
               {
                   Id = x.Id,
                   Email = x.Email,
                   FirstName = x.FirstName,
                   LastName = x.LastName,     
                  // DogName = x.DogName
               }).ToList();
            return Ok(customersModelList);
        }
        
        /// <summary>
        /// Возвращает клиента по ID
        /// </summary>
        /// <param name="id">идентификатор клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(int id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await customerRepository.GetByIdAsync(id);
            var response = new CustomerResponse()
            {
                Id = id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };
            foreach (var promoCode in customer.PromoCodes)
            {
                var promoResponse = new PromoCodeShortResponse()
                {
                    BeginDate = promoCode.BeginDate.ToString("dd.MM.yyyy"),
                    EndDate = promoCode.EndDate.ToString("dd.MM.yyyy"),
                    Code = promoCode.Code,
                    Id = promoCode.Id,
                    PartnerName = promoCode.PartnerName,
                    ServiceInfo = promoCode.ServiceInfo
                };
                response.PromoCodes.Add(promoResponse);          
            }           

            foreach (var preference in customer.Preferences)
            {
                var preferenceResponse = new PreferenceShortResponse()
                {
                    Name = preference.Name
                };
                response.Preferences.Add(preferenceResponse);
            }

            return response;
        }
        
        /// <summary>
        /// Создаёт клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns>id созданного клиента</returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            
            //var customerId = await customerRepository.AddAsync(customer);
            foreach (var preferenceId in request.PreferenceIds)
            {
                var preference = await preferencesRepository.GetByIdAsync(preferenceId);
                if(preference!=null)
                    customer.Preferences.Add(preference);                
            }
            var id = await customerRepository.AddAsync(customer);

            return Ok(id);
        }
        
        /// <summary>
        /// Редактирует клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(int id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var customer = await customerRepository.GetByIdAsync(id);
            if(customer == null)
                return await CreateCustomerAsync(request);
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences.Clear();

            foreach (var preferenceId in request.PreferenceIds)
            {
                var preference = await preferencesRepository.GetByIdAsync(preferenceId);
                customer.Preferences.Add(preference);                
            }
            customerRepository.Update(customer);

            return Ok();
        }
        
        /// <summary>
        /// Удаляет клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            await customerRepository.DeleteAsync(id);
            return Ok();
        }
    }
}