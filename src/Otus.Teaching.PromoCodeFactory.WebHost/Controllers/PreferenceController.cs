﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController
    {
        private readonly IRepository<Preference> preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            this.preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceResponse>> GetPreferencesAsync()
        {
            var preferences = await preferenceRepository.GetAllAsync();

            var responses = preferences.Select(x => 
                new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                }
            ).ToList();

            return responses;
        }
    }
}
