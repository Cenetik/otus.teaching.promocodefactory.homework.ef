﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class EfServiceExtension
    {
        public static IServiceCollection InstallEfService(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<DatabaseContext>(b => b.UseSqlite(connectionString));
            return services;
        }
    }
}
