﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
            //var sql = this.Database.GenerateCreateScript();
        }

        DbSet<Employee> Employees { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<Preference> Preferences { get; set; }
        DbSet<PromoCode> PromoCodes { get; set; }
        //DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Customer>()
            //    .HasMany(u => u.PromoCodes)
            //    .WithOne(c => c.Customer);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new { cp.PreferenceId, cp.CustomerId });

            modelBuilder.Entity<Customer>()
                .HasMany(u => u.Preferences)
                .WithMany(m => m.Customers)
                .UsingEntity<CustomerPreference>();

            modelBuilder.Entity<Preference>()
               .HasMany(u => u.Customers)
               .WithMany(m => m.Preferences)
               .UsingEntity<CustomerPreference>();

            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(c => c.Customer)
            //    .WithMany(p => p.Preferences)
            //    .HasForeignKey(c => c.CustomerId);

            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(p => p.Preference)
            //    .WithMany(cp => cp.CustomerPreferences)
            //    .HasForeignKey(c => c.PreferenceId);

            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(20);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(20);
            modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(50);

            modelBuilder.Entity<Role>().Property(c => c.Name).HasMaxLength(50);
            modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(500);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(20);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(20);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(50);

            modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(20);

            modelBuilder.Entity<PromoCode>().Property(c => c.PartnerName).HasMaxLength(50);
            modelBuilder.Entity<PromoCode>().Property(c => c.ServiceInfo).HasMaxLength(150);
            modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(50);

            modelBuilder.Entity<Role>().HasData(Data.FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(Data.FakeDataFactory.Employees);
            modelBuilder.Entity<Customer>().HasData(Data.FakeDataFactory.Customers);           
            modelBuilder.Entity<Preference>().HasData(Data.FakeDataFactory.Preferences);
            modelBuilder.Entity<CustomerPreference>().HasData(Data.FakeDataFactory.CustomerPreferences);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}
