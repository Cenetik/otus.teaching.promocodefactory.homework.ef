﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DatabaseContextExtension
    {
        public static DatabaseContext RecreateDb(this DatabaseContext context)
        {
        //    context.Database.EnsureDeleted();
         // context.Database.EnsureCreated();

         //   FillData(context);
            return context;
        }

        private static void FillData(DatabaseContext context)
        {
            
            foreach (var role in Data.FakeDataFactory.Roles)
            {
                context.Set<Role>().Add(role);
            }            

            foreach (var customer in Data.FakeDataFactory.Customers)
            {
                context.Set<Customer>().Add(customer);
            }

            //foreach (var customerPreference in Data.FakeDataFactory.CustomerPreferences)
            //{
            //    context.Set<CustomerPreference>().Add(customerPreference);
            //}

            foreach (var employee in Data.FakeDataFactory.Employees)
            {
                context.Set<Employee>().Add(employee);
            }

            context.SaveChanges();
        }
    }
}
