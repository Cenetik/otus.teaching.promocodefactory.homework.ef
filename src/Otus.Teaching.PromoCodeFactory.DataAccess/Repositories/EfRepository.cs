﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbSet<T> _entitySet;
        private readonly DbContext context;

        public EfRepository(DatabaseContext context)
        {
            _entitySet = context.Set<T>();
            this.context = context;
        }

        public async Task<int> AddAsync(T entity)
        {
            await _entitySet.AddAsync(entity);
            await this.context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await _entitySet.FindAsync(id);
            if (entity != null)
            {
                _entitySet.Remove(entity);
                await this.context.SaveChangesAsync();
            }
        }

        public virtual IQueryable<T> GetAll()
        {
            return _entitySet;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await GetAll().ToListAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await _entitySet.FindAsync(id);
        }

        public void Update(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();            
        }
    }
}
